# README #
## Criando ambiente para os requerimentos ##
Etl e DataVis testados em Python 3.8.8
Segue comandos para replicação do ambiente:

>virtualenv projetoArquiveiRosario --python=python3.8.8 

>projetoArquiveiRosario \Scripts\activate

Utilize cd para navegar até a pasta com os arquivos deste projeto

>pip install –r requirements.txt

## Execução do ETL ##
Para executar o etl com argumento de data especifica utilize:

>python etl.py ARG1

ARG1 = data no formato YYYY-MM-DD
O etl pode ser executado para todas as datas disponiveis com

>python etl.py

## Abrindo o DataVis##
O jupiter notebook é necessário para abertura da visualização
O datavis pode ser aberto com:

>jupyter notebook Datavis.ipynb

Obs:Para os mapas da 1ª visualização, marque a opção Trusted Notebook do menu File