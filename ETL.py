from google.cloud import storage
from google.cloud import bigquery
from datetime import datetime
import sys
import pandas as pd
import numpy as np
import pandas_gbq

def pre_execute():
    ##conecta ao bucket de dados
    global client_source
    client_source = storage.Client("bi-psel").get_bucket("arquivei-austin-incidents")
    ##conecta ao bigquerry
    global client_destination
    ##cria a tabela destino caso ela nao exista
    client_destination = bigquery.Client("bi-psel")
    client_destination.query("CREATE TABLE IF NOT EXISTS `bi-psel.rafael_rosario.austin-incidents` ("
    "`Descript` STRING NOT NULL,`Date` DATE NOT NULL,`Time` TIME NOT NULL,`Address` STRING NOT NULL,`Longitude` FLOAT64 ,`Latitude` FLOAT64 ,`Location` STRING ,`Timestamp` TIMESTAMP NOT NULL);")
    ##salva o schema da table para uso futuro
    global job_config
    job_config = bigquery.LoadJobConfig(
        schema=[
            bigquery.SchemaField("descript", bigquery.enums.SqlTypeNames.STRING, mode="REQUIRED"),
            bigquery.SchemaField("date", bigquery.enums.SqlTypeNames.DATE, mode="REQUIRED"),
            bigquery.SchemaField("time", bigquery.enums.SqlTypeNames.TIME, mode="REQUIRED"),
            bigquery.SchemaField("address", bigquery.enums.SqlTypeNames.STRING, mode="REQUIRED"),
            bigquery.SchemaField("longitude", bigquery.enums.SqlTypeNames.FLOAT, mode="NULLABLE"),
            bigquery.SchemaField("latitude", bigquery.enums.SqlTypeNames.FLOAT, mode="NULLABLE"),
            bigquery.SchemaField("location", bigquery.enums.SqlTypeNames.STRING, mode="NULLABLE"),
            bigquery.SchemaField("timestamp", bigquery.enums.SqlTypeNames.TIMESTAMP, mode="REQUIRED")
        ]
    )
    ##gera uma lista de todas as datas ja presentes na tabela para comparacao
    sqlQuery = """
    SELECT Distinct Date FROM `bi-psel.rafael_rosario.austin-incidents` 
    """
    df_date_list = pandas_gbq.read_gbq(sqlQuery, project_id="bi-psel")
    global date_list
    date_list = df_date_list['Date'].astype("str").tolist()


def extract(file_date, choice):
    ##cria o apontamento para o dataframe que servira de stage area
    global df_stage
    ##valida se os dados com escolha de nao serem sobrescritos ja existem no banco
    if (file_date in date_list) and (choice.upper() == 'S'):
        print("\n Dia {} ja presente na base de dados.".format(file_date))
        return False
    ##tenta coletar os dados da origem com a data parametro
    try:
        df_stage = pd.read_json(client_source.get_blob(file_date + "/incidents.ndjson").download_as_string(),lines=True)
    except:
        print("Arquivo nao encontrado para a data {}".format(file_date))
        return False
    ##caso ja existam dados com esta data no banco, realiza a limpeza para reinsercao
    if file_date in date_list:
        client_destination.query("DELETE FROM `bi-psel.rafael_rosario.austin-incidents` WHERE Date = PARSE_DATE('%Y-%m-%d', '" + file_date + "') ")
    return True


def transform(file_date):
    ##adiciona coluna date com a data do nome do arquivo
    df_stage["date"] = file_date
    df_stage["date"] = pd.to_datetime(df_stage["date"], format="%Y-%m-%d")
    ##converte a hora para o tipo Time
    df_stage["time"] = pd.to_datetime(df_stage["time"], format="%H:%M:%S").dt.time
    ##converte os dados que podem ser nulos
    df_stage["longitude"] = pd.to_numeric(df_stage["longitude"], errors='coerce')
    df_stage["latitude"] = pd.to_numeric(df_stage["latitude"], errors='coerce')
    df_stage["location"] = df_stage["location"].replace("null", np.nan, inplace=True)
    ##descarta a uniquekey que nao existe na tabela destino
    df_stage.drop("unique_key", inplace=True, axis=1)


def load(file_date):
    # cria o job conectado a tabela destino utilizando as configuracoes de schema
    job = client_destination.load_table_from_dataframe(df_stage, "bi-psel.rafael_rosario.austin-incidents",job_config=job_config)
    job.result()
    # table = client_destination.get_table("bi-psel.rafael_rosario.austin-incidents")
    print("\nForam carregadas {} linhas do arquivo de data {} para a tabela {} ".format(len(df_stage.index), file_date,"bi-psel.rafael_rosario.austin-incidents"))


def etl(file_date, choice):
    ##extract
    if (not extract(file_date, choice)): return
    ##transform
    transform(file_date)
    ##load
    load(file_date)

#chama a funcao de pre_execute
pre_execute()
#separa a execucao com parametro de data e a de todas datas
try:
    # Valida a data passada como parametro e roda o etl no arquivo equivalente
    test = datetime.strptime(sys.argv[1], '%Y-%m-%d')
    etl(sys.argv[1], 'N')
except:
    choice = input("\n\n\nNão foi passado parametro de data ou o mesmo é invalido.\nDeseja iniciar a rotina para todas as datas presentes na origem? \n S/N: ")
    if choice.upper() == 'S':
        choice = input("\nDeseja subir apenas os dados de dias ainda não adicionados? \nEm caso negativo dados ja presentes serao SOBRESCREVIDOS \n S/N: ")
        #coleta a lista de files da origem
        all_files = client_source.list_blobs()
        #aplica etl para todas files na origem
        for file in all_files:
            etl(file.name[:-17], choice)
        print("\nRotina encerrada")
    else:
        print("\nA rotina será encerrada.\nPara enviar parametro de data utilize o formato YYYY-MM-DD")